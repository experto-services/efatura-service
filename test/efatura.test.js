
var expect = require('chai').expect
var eFatura = require('../src/controller/efatura.controller')
const { v4: uuidv4 } = require('uuid');

describe('EFatura', function () {
    describe('Fatura', function () {
        it('fatura bulunur', async () => {
            let faturaKesim = {
                fatura: {
                    key: uuidv4(),
                    duzenlemeTarihi: '2021-05-01',
                    faturaTuru: 'SATIS',
                    aciklama: 'Fatura aciklamasi',
                    kesenFirma: {
                        vkn: '3810685526',
                        vd: 'Kesen VD',
                        unvan: 'KESEN FIRMA',
                        adres: 'Kesen Frima Adresi sokak',
                        ilce: 'Basaksehir',
                        il: 'Istanbul',
                        ulke: 'Turkiye',
                        kontakTelefon: '5320000',
                        kontakMail: 'kesen@kesen.com.tr'
                    },
                    firma: {
                        efaturaTCKNKullan: false,
                        vergiNo: '3810685527',
                        unvan: 'Alan Firma',
                        adres: 'alan Firma sokak',
                        ilce: 'Fatih',
                        il: 'Istanbul',
                        ulke: 'Turkiye',
                        vergiDairesi: 'Alan VD'
                    },
                    kalemler: [
                        {
                            miktar: 1,
                            birimFiyat: 100,
                            kdvOran: 18,
                            urun: {
                                adi: 'URUN'
                            },
                            aciklama: 'aciklma'
                        }
                    ]
                },
                baglanti: {
                    efaturaUrl: 'https://erpefaturatest.cs.com.tr:8043/efatura/ws/connectorService?wsdl',
                    efaturaUser: '3810685526',
                    efaturaPwd: 'Experto2021',
                },
                faturaKodu: 'TST',
                sonKesilenFaturaNo: 'TST2021000000494'
            }
            eFatura.sendToBank(faturaKesim).then(resp => {
                expect(resp.error).to.equal(null)
            })
        })
    })
})

