
var expect = require('chai').expect
var FaturaHelper = require('../src/helpers/faturano.helper')
const { v4: uuidv4 } = require('uuid');

describe('EFatura', function () {
    it('fatura no uret', function () {

        let kesimler = [
        ]

        let f1 = FaturaHelper.calcFaturaNo('20210601', kesimler)
        expect(f1.seri).to.equals('AAA')
        //expect(f1.sira).to.equals(1)
        expect(f1.gun).to.equals('20210601')
        console.log(JSON.stringify(f1, null, 2))

        let f2 = FaturaHelper.calcFaturaNo('20210610', kesimler)
        expect(f2.seri).to.equals('AAA')
        //expect(f2.sira).to.equals(2)
        expect(f2.gun).to.equals('20210610')
        console.log(JSON.stringify(f2, null, 2))

        let f3 = FaturaHelper.calcFaturaNo('20210612', kesimler)
        expect(f3.seri).to.equals('AAA')
        //expect(f3.sira).to.equals(3)
        expect(f3.gun).to.equals('20210612')
        console.log(JSON.stringify(f3, null, 2))

        let f4 = FaturaHelper.calcFaturaNo('20210605', kesimler)
        expect(f4.seri).to.equals('BBB')
        //expect(f4.sira).to.equals(1)
        expect(f4.gun).to.equals('20210605')
        console.log(JSON.stringify(f4, null, 2))

        let f5 = FaturaHelper.calcFaturaNo('20210606', kesimler)
        expect(f5.seri).to.equals('BBB')
        //expect(f5.sira).to.equals(2)
        expect(f5.gun).to.equals('20210606')
        console.log(JSON.stringify(f5, null, 2))

        f5 = FaturaHelper.calcFaturaNo('20210603', kesimler)
        expect(f5.seri).to.equals('CCC')
        //expect(f5.sira).to.equals(1)
        expect(f5.gun).to.equals('20210603')
        console.log(JSON.stringify(f5, null, 2))

        f5 = FaturaHelper.calcFaturaNo('20210606', kesimler)
        expect(f5.seri).to.equals('BBB')
        //expect(f5.sira).to.equals(3)
        expect(f5.gun).to.equals('20210606')
        console.log(JSON.stringify(f5, null, 2))

        f5 = FaturaHelper.calcFaturaNo('20210603', kesimler)
        expect(f5.seri).to.equals('CCC')
        //expect(f5.sira).to.equals(2)
        expect(f5.gun).to.equals('20210603')
        console.log(JSON.stringify(f5, null, 2))

        f5 = FaturaHelper.calcFaturaNo('20210613', kesimler)
        expect(f5.seri).to.equals('AAA')
        //expect(f5.sira).to.equals(4)
        expect(f5.gun).to.equals('20210613')
        console.log(JSON.stringify(f5, null, 2))

        f5 = FaturaHelper.calcFaturaNo('20210603', kesimler)
        expect(f5.seri).to.equals('CCC')
        expect(f5.gun).to.equals('20210603')
        console.log(JSON.stringify(f5, null, 2))

        f5 = FaturaHelper.calcFaturaNo('20210602', kesimler)
        expect(f5.seri).to.equals('DDD')
        expect(f5.gun).to.equals('20210602')
        console.log(JSON.stringify(f5, null, 2))
    })
})
