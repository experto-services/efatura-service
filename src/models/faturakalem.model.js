const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key                 : String,
    vfatura             : String,
    vurun               : String,
    aciklama            : String,
    birimFiyat          : Number,
    kdvOran             : Number,
    kdv                 : Number,
    miktar              : Number,
    toplam              : Number
},
{
    toJSON  : { virtuals: true },
    toObject: { virtuals: true }
})

SchemaModel.virtual('fatura', {
    ref         : 'Firma',
    localField  : 'vfirma',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('urun', {
    ref         : 'Urun',
    localField  : 'vurun',
    foreignField: 'key',
    justOne     : true
})

module.exports = mongoose.models.FaturaKalem || mongoose.model("FaturaKalem", SchemaModel);
