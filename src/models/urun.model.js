const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key                       : { type: String, required: true },
    adi                       : { type: String, required: true },
    aktifMi                   : { type: Boolean, default: true },
    onOdemeTutari             : { type: Number },
    onOdemeTutariVar          : { type: Boolean, default: false},
    basariTutari              : { type: Number },
    basariTutariVar           : { type: Boolean, default: false},
    sabitTutar                : { type: Number },
    sabitTutarVar             : { type: Boolean, default: false},
    yuzdeTutari               : { type: Number },
    yuzdeTutariVar            : { type: Boolean, default: false },
    raporBasiOdemeTutari      : { type: Number },
    raporBasiOdemeTutariVar   : { type: Boolean, default: false },
    kdvOrani                  : { type: Number },
    urunRenk                  : { type: String },
    kisaKod                   : { type: String },
    createdAt                 : { type: Date, default: Date.now },
    updatedAt                 : { type: Date },
})

module.exports = mongoose.models.Urun || mongoose.model("Urun", SchemaModel);
