let seriler = ['AAA', 'BBB', 'CCC', 'DDD', 'EEE', 'FFF']

const calcFaturaSeri = (faturaGunu, kesimler) => {
    let i = 0
    let kesim
    for (i = 0; i < kesimler.length; ++i) {
        kesim = kesimler[i]
        if (faturaGunu >= kesim.gun) {
            kesim.gun = faturaGunu
            //kesim.sira++
            return {
                seri: kesim.seri,
                //sira: kesim.sira,
                gun: kesim.gun
            }
        }
    }
    let k = {
        seri: seriler[i],
        //sira: 1,
        gun: faturaGunu
    }
    kesimler.push(k)
    return k
}

module.exports = {
    calcFaturaSeri
}