const amqp = require("amqplib");
const { sendEFatura, gidenBelgeleriSorgula, gelenFaturalariAl } = require('../controller/efatura.controller')
const { send } = require('../controller/main.controller')
const { logger } = require('./loggerService')
const { EXCHANGE, QUEUE } = require('../resources/constants');
const PREFETCH_COUNT = parseInt(process.env.PREFETCH_COUNT) || 1;
const MQ_HOST = process.env.AMQP_HOST || 'localhost';
const MQ_URL = `amqp://${MQ_HOST}:5672`;
let orderChannel = null;


const messageRouter = async (rawMessage, channel) => {
    let message = JSON.parse(rawMessage.content.toString());

    if (message.command === 'FATURA_KES') {
        logger.info('--------------Fatura Kesme Islemi------------------')
        //let result = await sendEFatura(message.payload)
        try {
            let result = await send(message.payload)
            //if (result.error) {
            //    logger.log('crit', `Fatura gonderim basarisiz. ${result.error.code} ${result.error.message}`)
            //}
            //else {
            //
            //}
        }
        catch (e) {
            logger.log(`crit`, `Hata: ${e}`)
        }
    }
    else if (message.command === 'FATURA_SORGULA') {
        logger.info('-------------Fatura Sorgulama Islemi---------------')
        let result = await gidenBelgeleriSorgula(message.payload)
        if (result.error) {
            logger.log('crit', `Giden belgeleri sorgulama basarisiz. ${result.error.code} ${result.error.message}`)
        }
    }
    else if (message.command === 'GELEN_FATURALARI_AL') {
        logger.info('-------------Gelen Faturalari Al Islemi------------')
        let result = await gelenFaturalariAl(message.payload)
        if (result.error) {
            logger.log('crit', `Giden belgeleri sorgulama basarisiz. ${result.error.code} ${result.error.message}`)
        }
    }
    else {
        logger.log('crit', '------------ Bilinmeyen Islem -----------------')
        logger.log('crit', JSON.stringify(rawMessage.content, null, 2))
    }
    channel.ack(rawMessage)
}
/**
 * Connect to RabbitMQ and consumer orders
 */
const amqpConnectAndConsume = async () => {
    try {
        const mqConnection = await amqp.connect(MQ_URL);
        logger.info(`AMQP - connection established at ${MQ_URL}`)

        orderChannel = await mqConnection.createChannel();

        await orderChannel.assertExchange(EXCHANGE, 'fanout', {
            durable: false
        });

        // Ensure that the queue exists or create one if it doesn't
        await orderChannel.assertQueue(QUEUE);
        await orderChannel.bindQueue(QUEUE, EXCHANGE, '');


        // Only send <PREFETCH_COUNT> emails at a time
        orderChannel.prefetch(PREFETCH_COUNT);

        orderChannel.consume(QUEUE, order => {
            messageRouter(order, orderChannel)
            //sendEFatura(order, orderChannel)
        });
    }
    catch (ex) {
        logger.log('fatal', `AMQP - ${ex}`);
        process.exit();
    }
}

module.exports = {
    amqpConnectAndConsume: amqpConnectAndConsume
}