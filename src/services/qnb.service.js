const md5 = require('md5')
const soap = require("soap");
var _client = null

exports.createClient = (url, user, password) => {
    return new Promise((resolve, reject) => {
        const wsSecurity = new soap.WSSecurity(
            user, 
            password, 
            {})
            //url = 'https://efaturaconnector.efinans.com.tr/connector/ws/connectorService?WSDL'
        soap.createClient(url, (err, client) => {
            if (err) {
                reject(err)
            }
            else {
                client.setSecurity(wsSecurity);
                _client = client

                resolve({
                    faturaNoUret,
                    efaturaKullanicisiMi,
                    belgeGonder,
                    gidenBelgeSorgula,
                    gidenBelgeleriIndir,
                    gelenBelgeleriAl,

                    // EArsiv
                    eArsiv_efaturaKullanicisi,
                    faturaOlustur,
                    faturaSorgula,
                    faturaIptalEt,
                    eArsiv_faturaNoUret,

                })
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const faturaNoUret = (vkn, faturaKodu) => {
    return new Promise((resolve, reject) => {
        _client.faturaNoUret({ vknTckn: vkn, faturaKodu: faturaKodu }, (err, response) => {
            if (err) {
                reject(err.message)
            }
            else {
                resolve(response.return)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const efaturaKullanicisiMi = (vkn) => {
    return new Promise((resolve, reject) => {
        _client.efaturaKullaniciBilgisi({ vergiTcKimlikNo: vkn }, (err, response) => {
            if (err) {
                reject(err.message)
            }
            else {
                resolve(response.return)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const belgeGonder = ({ ubl, vkn, key }) => {
    return new Promise((resolve, reject) => {
        let ublBase64 = Buffer.from(ubl).toString('base64')
        let hash = md5(ubl)

        _client.belgeGonder({
            vergiTcKimlikNo: vkn,
            belgeTuru: 'FATURA_UBL',
            belgeNo: key, //fatura.uuid,
            veri: ublBase64,
            belgeHash: hash,
            mimeType: 'application/xml'
        }, (err, response) => {
            if (err) {
                reject(err.message)
            }
            else {
                resolve(response.belgeOid)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------
const gidenBelgeSorgula = (gonderenVkn, belgeOid) => {
    return new Promise((resolve, reject) => {
        _client.gidenBelgeDurumSorgula({
            vergiTcKimlikNo: gonderenVkn,
            belgeOid: belgeOid,
        }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(res.return)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const gidenBelgeleriIndir = (data) => {
    return new Promise((resolve, reject) => {
        _client.gidenBelgeleriIndir(data, (e, res) => {
            if (e) {
                reject(e)
            }
            else {
                resolve(res.return)
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const gelenBelgeleriAl = ({ vkn, sonAlinanBelgeSiraNo, belgeTuru }) => {
    return new Promise((resolve, reject) => {
        _client.gelenBelgeleriAl({
            vergiTcKimlikNo: vkn,
            sonAlinanBelgeSiraNumarasi: sonAlinanBelgeSiraNo,
            belgeTuru: belgeTuru
        }, (err, res) => {
            if (err) {
                reject(err)
            }
            else {
                if (res == null) {
                    resolve([])
                }
                else {
                    resolve(res.return)
                }
            }
        })
    })
}

//----------------------------------------------------------------------------------------------------------

const eArsiv_efaturaKullanicisi = async (vkn) => {
    let res = await _client.efaturaKullanicisiAsync({vergiTcKimlikNo: vkn})
    return res[0].return
/*
    return new Promise((resolve, reject) => {
        _client.efaturaKullanicisi({ vergiTcKimlikNo: vkn }, (err, response) => {
            if (err) {
                reject(err)
            }
            else {
                resolve(response.return)
            }
        })
    })
    */
}

//----------------------------------------------------------------------------------------------------------

const eArsiv_faturaNoUret = async ({ islemId, vkn, faturaSeri }) => {
    let res = await _client.faturaNoUretAsync({
        input: JSON.stringify({ islemId, vkn, faturaSeri })
    })
    if (res[0].return.resultCode == 'AE00000') {
        return res[0].output
    }
    else {
        //console.log(JSON.stringify(res[0], null, 2))
        //reject(res[0].return.resultText)
    }


    /*
        return new Promise((resolve, reject) => {
            _client.faturaNoUret({ input: JSON.stringify({
                islemId: islemId, 
                vkn: vkn,
                //kasa: 'DFLT',
                //sube: 'DFLT',
                //tarih: '20210621',
                //faturaSeri: 'CCC'
            })
            }, (res) => {
                console.log(res)
                if (res.return.code =='AE00000') {
                    resolve(res.output)
                }
                else {
                    reject(res.return.resultText)
                }
            })
        })
    */
}

//----------------------------------------------------------------------------------------------------------

const faturaOlustur = async ({ islemId, vkn, key, ubl }) => {
    let ublBase64 = Buffer.from(ubl).toString('base64')

    let res = await _client.faturaOlusturAsync({
        input: JSON.stringify({
            donenBelgeFormati: 3,
            islemId: islemId,
            vkn: vkn,
            sube: 'DFLT',
            kasa: 'DFLT',
            numaraVerilsinMi: 1,
            faturaSeri: "EX"
            //yerelFaturaNo: key
        }),

        fatura: {
            belgeFormati: 'UBL',
            belgeIcerigi: ublBase64
        }
    })
    if (res[0].return.resultCode == 'AE00000') {
        let islemIdVal = res[0].return.resultExtra.entry.find(e => e.key.$value == 'islemID')
        let islemId = ''
        if (islemIdVal) {
            islemId = islemIdVal.value.$value
        }

        let faturaUrlVal = res[0].return.resultExtra.entry.find(e => e.key.$value == 'faturaURL')
        let faturaUrl = ''
        if (faturaUrlVal) {
            faturaUrl = faturaUrlVal.value.$value
        }

        let uuidVal = res[0].return.resultExtra.entry.find(e => e.key.$value == 'uuid')
        let uuid = ''
        if (uuidVal) {
            uuid = uuidVal.value.$value
        }

        let faturaNoVal = res[0].return.resultExtra.entry.find(e => e.key.$value == 'faturaNo')
        let faturaNo = ''
        if (faturaNoVal) {
            faturaNo = faturaNoVal.value.$value
        }

        return { 
            result: res[0].output,
            islemId : islemId || '',
            faturaURL: faturaUrl || '',
            uuid: uuid || '',
            faturaNo : faturaNo || ''
        }
    }
    return {
        error: {
            code: res[0].return.resultCode,
            message: res[0].return.resultText
        }
    }
}

//----------------------------------------------------------------------------------------------------------

const faturaSorgula = () => {

}

//----------------------------------------------------------------------------------------------------------

const faturaIptalEt = () => {

}

//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------
