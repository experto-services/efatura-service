const mongoose = require('mongoose');
const { logger } = require('./loggerService');
// environment variables
const MONGO_CONTAINER_NAME = process.env.MONGO_HOST || 'localhost';
const MONGO_URI = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PWD}@${process.env.MONGO_HOST}/ExpertoDb`;

const mongoConnect = () => {
    mongoose.Promise = global.Promise;
    mongoose.connect(MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }, (err) => {  
        if(err) {
            console.error('Mongo ERROR ' + err)
        }
    })
    mongoose.connection.on('connected', function () {  
        logger.log('info',`Mongoose - connection established at ${process.env.MONGO_HOST}`);
    }); 
    
    // If the connection throws an error
    mongoose.connection.on('error',function (err) {  
        logger.log('fatal',`Mongoose - connection error: ${process.env.MONGO_HOST}`);
    }); 
    
    // When the connection is disconnected
    mongoose.connection.on('disconnected', function () {  
        logger.log('fatal',`Mongoose - disconnected: ${process.env.MONGO_HOST}`);
    });
}

const getTenant = (tenantId) => {

}

const getFatura = (faturaId) => {

}

const updateFatura = (fatura) => {

}

module.exports = {
    mongoConnect: mongoConnect,
    getTenant,
    getFatura,
    updateFatura
}