const { logger } = require('../services/loggerService');
const { sendEFatura } = require('./efatura.controller')
const { sendEArsiv } = require('./earsiv.controller')
const TenantModel = require('../models/tenant.model');
const FirmaModel = require('../models/firma.model');
const FaturaKalemModel = require('../models/faturakalem.model');
const FaturaModel = require('../models/fatura.model');
const UrunModel = require('../models/urun.model');
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const FaturaHelper = require('../helpers/faturano.helper')

//---------------------------------------------------------------------------------------------------

const prepareFatura = async (efatura) => {
    try {
        // Faturayi Bul
        let fatura = await FaturaModel
            .findOne({ key: efatura.key })
            .populate('firma')
            .populate('kesenFirma')
            .lean(true)
            .exec()
        if (!fatura) {
            return {
                error: {
                    code: -1,
                    message: `Fatura bulunamadi. ${efatura.key}`
                }
            }
        }
        //if (fatura.status != 'YeniKayit') {
        //    logger.log('crit', `Fatura Yeni Kayit degil. FaturaID: ${fatura._id}`)
        //    return {
        //        error: -32,
        //        message: `Fatura YeniKayit degil ${fatura._id}`
        //    }
        //    // TODO
        //}
        let kalemler = await FaturaKalemModel.find({ vfatura: fatura.key }).populate('urun').lean(true).exec()


        logger.info(`Fatura bulundu`)
        logger.info(`${JSON.stringify(fatura, null, 2)}`)

        // Hangi seri kullanilacak
//        let gecmisTarihli = false
//        let faturaKodu = ''
//        //if (moment(fatura.duzenlemeTarihi).startOf('day').isBefore(moment().startOf('day'))) {
//        //    gecmisTarihli = true
//        //}
//        let sonFaturaNo = gecmisTarihli ? fatura.kesenFirma.sonGecmisTarihFaturaNo : fatura.kesenFirma.sonFaturaNo
//        //faturaKodu = gecmisTarihli ? fatura.kesenFirma.gecmisTarihFaturaKodu : fatura.kesenFirma.faturaKodu
//        //logger.info(`Gecmis Tarihli: ${gecmisTarihli}`)
//        //logger.info(`Fatura Kodu: ${faturaKodu}`)
//
//        let kesilenFaturaSerileri = fatura.kesenFirma.faturaSerileri.sort((a, b) => {
//            return a.gun < b.gun
//        })
//
//        let kesimSerisi = FaturaHelper.calcFaturaSeri(moment(fatura.duzenlemeTarihi).format('YYYYMMDD'), kesilenFaturaSerileri)
//        console.log(JSON.stringify(kesimSerisi, null, 2))
//        faturaKodu = kesimSerisi.seri


        if (kalemler.length == 0 ||
            (!fatura.firma.efaturaTCKNKullan && fatura.firma.vergiDairesi.trim().length == 0) ||
            fatura.firma.vergiNo.trim().length == 0 ||
            fatura.firma.unvan.trim().length == 0 
            || (
                (fatura.firma.faturaAdres && fatura.firma.faturaAdres.trim().length == 0) && 
                (fatura.firma.adres && fatura.firma.adres.trim().lentgh == 0)) 
            //fatura.firma.eFaturaMi == false
            ) {
                console.log(`${fatura.firma.unvan}`)
                throw `Eksik kayit ${fatura.firma.unvan} ${fatura.key}`
            }

        let faturaKesim = {
            fatura: {
                //key: uuidv4(), //fatura.key,
                key: fatura.key,
                duzenlemeTarihi: fatura.duzenlemeTarihi,
                faturaTuru: fatura.faturaTuru || 'SATIS',
                aciklama: fatura.aciklama,
                kesenFirma: {
                    vkn: fatura.kesenFirma.vkn,
                    vd: fatura.kesenFirma.vd,
                    unvan: fatura.kesenFirma.unvan,
                    adres: fatura.kesenFirma.adres,
                    ilce: fatura.kesenFirma.ilce,
                    il: fatura.kesenFirma.il,
                    ulke: fatura.kesenFirma.ulke,
                    kontakTelefon: fatura.kesenFirma.kontakTelefon,
                    kontakMail: fatura.kesenFirma.kontakMail,
                    key: fatura.kesenFirma.key
                },
                firma: {
                    efaturaTCKNKullan: fatura.firma.efaturaTCKNKullan,
                    vergiNo: fatura.firma.vergiNo,
                    unvan: fatura.firma.unvan,
                    adres: fatura.firma.faturaAdres || fatura.firma.adres,
                    ilce: fatura.firma.ilce,
                    il: fatura.firma.il,
                    ulke: fatura.firma.ulke,
                    vergiDairesi: fatura.firma.vergiDairesi,
                    eFaturaMi: fatura.firma.eFaturaMi,
                    efaturaTCKN: fatura.firma.efaturaTCKN,
                    efaturaTCKNAdi: fatura.firma.efaturaTCKNAdi,
                    efaturaTCKNSoyadi: fatura.firma.efaturaTCKNSoyadi
                },
                kalemler: kalemler.map(k => {
                    return {
                        miktar: k.miktar,
                        birimFiyat: k.birimFiyat,
                        kdvOran: k.kdvOran,
                        urun: {
                            adi: k.urun ? k.urun.adi : ''
                        },
                        aciklama: k.aciklama
                    }
                })
            },
            baglanti: {
                url: fatura.firma.eFaturaMi ? fatura.kesenFirma.efaturaUrl : fatura.kesenFirma.earsivUrl,
                user: fatura.firma.eFaturaMi ? fatura.kesenFirma.efaturaUser : fatura.kesenFirma.earsivUser,
                pwd: fatura.firma.eFaturaMi ? fatura.kesenFirma.efaturaPwd : fatura.kesenFirma.earsivPwd,
                //url: fatura.kesenFirma.efaturaUrl,
                //user: fatura.kesenFirma.efaturaUser,
                //pwd: fatura.kesenFirma.efaturaPwd,
            },
            //faturaKodu: faturaKodu,
            //sonKesilenFaturaNo: sonFaturaNo
        }

        return faturaKesim
    }
    catch (e) {
        logger.log('crit', e)
    }
}

//---------------------------------------------------------------------------------------------------

const updateFatura = async (kesim, updateData) => {
    logger.info(`Fatura guncelleniyor. ${kesim.fatura.key}`)
    await FaturaModel.findOneAndUpdate({ key: kesim.fatura.key }, updateData).exec()

    //let tenant = await TenantModel.findOne({key: kesim.fatura.kesenFirma.key}).lean(true).exec()
    //let kesimSerisi = tenant.faturaSerileri.find(s => s.seri == kesim.faturaKodu)
    //if (kesimSerisi) {
    //    kesimSerisi.gun = moment(kesim.fatura.duzenlemeTarihi).format('YYYYMMDD')
    //}
    //else {
    //    tenant.faturaSerileri.push({
    //        seri: kesim.faturaKodu,
    //        gun: moment(kesim.fatura.duzenlemeTarihi).format('YYYYMMDD')
    //    })
    //}
    //
    //await TenantModel.findOneAndUpdate({key: tenant.key}, {faturaSerileri : tenant.faturaSerileri}).exec()
}

//---------------------------------------------------------------------------------------------------

const send = async (payload) => {
    //try {
        logger.info(`Fatura gonderim talebi geldi. ${JSON.stringify(payload)}`)
        let fatura = await prepareFatura(payload)
        if (!fatura) {
            logger.log('crit', 'Fatura bulunamadi')
            return
        }
        if (fatura.error) {
            return fatura.error
        }
        logger.log(`info`, `DEBUG: ${JSON.stringify(fatura, null, 2)}`)

        let resp
        let updateData
        if (fatura.fatura.firma.eFaturaMi) {
            logger.info(`EFatura kesilecek`)
            resp = await sendEFatura(fatura)
            if (resp.error) {
                logger.log(`crit`, `Hata ${JSON.stringify(resp.error)}`)
                //return resp.error
                updateData = {
                    hataKodu: '' + resp.error.code,
                    hataMesaji: resp.error.message,
                    status: 'Hata'
                }
            }
            else {
                updateData = {
                    belgeOid: resp.belge.belgeOid,
                    alimTarihi: resp.belge.alimTarihi,
                    belgeNo: resp.belge.belgeNo,
                    ettn: resp.belge.ettn,
                    olusturulmaTarihi: resp.belge.olusturulmaTarihi,
                    faturaNo: resp.belge.faturaNo,
                    ubl: resp.belge.ubl,
                    status: 'Gonderildi'
                }
            }
        }
        else {
            logger.info(`EArsiv kesilecek ${JSON.stringify(fatura, null, 2)}`)
            resp = await sendEArsiv(fatura)
            updateData = {
                ...resp.belge, //ubl: resp.ubl,
                status: 'Gonderildi'
            }
        }

        //if (resp.error) {
        //    logger.log(`crit`, `Hata: ${JSON.stringify(resp.error)}`)
        //    return
        //}
        logger.log(`info`, `DEBUG: ${JSON.stringify(fatura, null, 2)}`)
        logger.log(`info`, `Fatura guncelleme: ${fatura.fatura.key} ${JSON.stringify(updateData)}`)
        await updateFatura(fatura, updateData)
        logger.log(`info`, `Bitti`)
        return { error: null }
    //}
    //catch (e) {
    //    logger.log(`crit`, `Hata: ${e}`)
    //}
}

//---------------------------------------------------------------------------------------------------

module.exports = {
    send
}