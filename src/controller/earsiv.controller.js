process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const moment = require('moment');
const TenantModel = require('../models/tenant.model');
const FirmaModel = require('../models/firma.model');
const FaturaKalemModel = require('../models/faturakalem.model');
const FaturaModel = require('../models/fatura.model');
const UrunModel = require('../models/urun.model');
const { logger } = require('../services/loggerService');
const QnbService = require('../services/qnb.service')
const { makeUbl, makeUbleArsiv } = require('../helpers/ubl.helper')
const { v4: uuidv4 } = require('uuid');

const sendEArsiv = async (kesim) => {
    try {
        logger.info(`QNB baglantisi kuruluyor: ${kesim.baglanti.url} ${kesim.baglanti.user} ${kesim.baglanti.pwd}`)

        let client = await QnbService.createClient(kesim.baglanti.url, kesim.baglanti.user, kesim.baglanti.pwd)
        if (!client) {
            logger.log('crit', `QNB baglantisi kurulamadi. ${kesim.baglanti.url} ${kesim.baglanti.user} ${kesim.baglanti.pwd}`)
            // TODO
            return {
                error: {
                    code: -1,
                    message: `Baglanti kurulamadi ${kesim.baglanti.url} ${kesim.baglanti.user} ${kesim.baglanti.pwd}`
                }
            }
        }
        logger.info(`Qnb baglantisi kuruldu`)

        // Karşı taraf eFatura mı, değilse eArşiv kesilir.
        let eArsivMi = await client.eArsiv_efaturaKullanicisi(kesim.fatura.firma.vergiNo)
        logger.info(`EArsiv mi: ${kesim.fatura.firma.vergiNo} ${eArsivMi}`)
        //if (!eArsivMi) {
        //    logger.log('crit', `Firma EArsiv degil ${kesim.fatura.firma.vergiNo}`)
        //    return {
        //        error: {
        //            code: -20,
        //            message: `Firma EArsiv degil ${kesim.fatura.firma.vergiNo}`
        //        }
        //    }
        //}


        // Yeni Fatura no al
        //let faturaNo = ''
        //let tryLimit = 3
        //do {
        //    logger.info(`FaturaNoUret(): Kesen Firma VKN:${kesim.fatura.kesenFirma.vkn}, Fatura Kodu: ${kesim.faturaKodu}`)
        //    faturaNo = await client.eArsiv_faturaNoUret({
        //        islemId: uuidv4(),
        //        vkn: kesim.fatura.kesenFirma.vkn,
        //        faturaSeri: 'CCC'                
        //    })
        //    if (--tryLimit <= 0) {
        //        // 
        //        logger.log('crit', `Yeni Fatura numarasi alinamiyor. Alinan yeni fatura numarasi: ${faturaNo}, son fatura no: ${kesim.sonKesilanFaturaNo}`)
        //        return {
        //            error: {
        //                code: -2,
        //                message: `Gelen Fatura kullanilmis ${faturaNo}`
        //            }
        //        }
        //    }
        //} while (faturaNo == kesim.sonKesilanFaturaNo)
        //logger.info(`Fatura duzenleme tarihi: ${moment(kesim.fatura.duzenlemeTarihi).format('DD.MM.YYYY')}, Fatura Seri: ${kesim.faturaKodu}, Son Fatura No: ${kesim.sonKesilenFaturaNo}, Alinan Fatura No: ${faturaNo}`)

        // Yeni alinan Fatura no atanir
        //kesim.fatura.faturaNo = faturaNo

        let ubl = makeUbleArsiv(kesim.fatura)
        let islemId = uuidv4()
        logger.info(`EArsiv gonderiliyor. IslemId: ${islemId}`)
        logger.info(`EArsiv UBL. ${ubl}`)

        let faturaSonuc = await client.faturaOlustur({
            islemId: islemId,
            vkn: kesim.fatura.kesenFirma.vkn,
            yerelFaturaNo: kesim.fatura.key,
            ubl: ubl
        })
        if (faturaSonuc.error) {
            logger.info(`EArsiv gonderim hata. ${faturaSonuc.error.code} ${faturaSonuc.error.message}`)
            return faturaSonuc.error
        }
        logger.info(`EArsiv gonderimi basarili. ${islemId}`)
        return {
            belge: {
                ubl: ubl,
                gidenBelgePdf: faturaSonuc.result.belgeIcerigi,
                islemId: faturaSonuc.islemId,
                faturaURL: faturaSonuc.faturaURL,
                uuid: faturaSonuc.uuid,
                faturaNo: faturaSonuc.faturaNo,
            }
        }
    }
    catch (e) {
        logger.log(`crit`, e)
    }
}

module.exports = {
    sendEArsiv
}