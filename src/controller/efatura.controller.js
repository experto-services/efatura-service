process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

const moment = require('moment');
const TenantModel = require('../models/tenant.model');
const FirmaModel = require('../models/firma.model');
const FaturaKalemModel = require('../models/faturakalem.model');
const FaturaModel = require('../models/fatura.model');
const UrunModel = require('../models/urun.model');
const { logger } = require('../services/loggerService');
const QnbService = require('../services/qnb.service')
const { makeUbl } = require('../helpers/ubl.helper')
const { v4: uuidv4 } = require('uuid');
const { sendToBank: SendEArsivToBank } = require('../controller/earsiv.controller')

//-----------------------------------------------------------------------------------------------------------------------------------------
/*
const prepareFatura = async (efatura) => {
    try {
        // Faturayi Bul
        let fatura = await FaturaModel
            .findOne({ key: efatura.key })
            .populate('firma')
            .populate('kesenFirma')
            .lean(true)
            .exec()
        if (!fatura) {
            return {
                error: {
                    code: -1,
                    message: `Fatura bulunamadi. ${efatura.key}`
                }
            }
        }
        if (fatura.status != 'KesmeyeHazir') {
            logger.log('crit', `Fatura Kesmeye hazir degil. FaturaID: ${fatura._id}`)
            // TODO
        }
        let kalemler = await FaturaKalemModel.find({ vfatura: fatura.key }).populate('urun').lean(true).exec()


        logger.info(`Fatura bulundu`)
        logger.info(`${JSON.stringify(fatura, null, 2)}`)

        // Hangi seri kullanilacak
        let gecmisTarihli = false
        let faturaKodu = ''
        if (moment(fatura.duzenlemeTarihi).startOf('day').isBefore(moment().startOf('day'))) {
            gecmisTarihli = true
        }
        let sonFaturaNo = gecmisTarihli ? fatura.kesenFirma.sonGecmisTarihFaturaNo : fatura.kesenFirma.sonFaturaNo
        faturaKodu = gecmisTarihli ? fatura.kesenFirma.gecmisTarihFaturaKodu : fatura.kesenFirma.faturaKodu
        logger.info(`Gecmis Tarihli: ${gecmisTarihli}`)
        logger.info(`Fatura Kodu: ${faturaKodu}`)

        let faturaKesim = {
            fatura: {
                key: uuidv4(), //fatura.key,
                duzenlemeTarihi: fatura.duzenlemeTarihi,
                faturaTuru: fatura.faturaTuru || 'SATIS',
                aciklama: fatura.aciklama,
                kesenFirma: {
                    vkn: fatura.kesenFirma.vkn,
                    vd: fatura.kesenFirma.vd,
                    unvan: fatura.kesenFirma.unvan,
                    adres: fatura.kesenFirma.adres,
                    ilce: fatura.kesenFirma.ilce,
                    il: fatura.kesenFirma.il,
                    ulke: fatura.kesenFirma.ulke,
                    kontakTelefon: fatura.kesenFirma.kontakTelefon,
                    kontakMail: fatura.kesenFirma.kontakMail,
                },
                firma: {
                    efaturaTCKNKullan: fatura.firma.efaturaTCKNKullan,
                    vergiNo: fatura.firma.vergiNo,
                    unvan: fatura.firma.unvan,
                    adres: fatura.firma.adres,
                    ilce: fatura.firma.ilce,
                    il: fatura.firma.il,
                    ulke: fatura.firma.ulke,
                    vergiDairesi: fatura.firma.vergiDairesi,
                    eFaturaMi: fatura.firma.eFaturaMi
                },
                kalemler: kalemler.map(k => {
                    return {
                        miktar: k.miktar,
                        birimFiyat: k.birimFiyat,
                        kdvOran: k.kdvOran,
                        urun: {
                            adi: k.urun ? k.urun.adi : ''
                        },
                        aciklama: k.aciklama
                    }
                })
            },
            baglanti: {
                efaturaUrl: fatura.firma.eFaturaMi ? fatura.kesenFirma.efaturaUrl : fatura.kesenFirma.earsivUrl,
                efaturaUser: fatura.firma.eFaturaMi ? fatura.kesenFirma.efaturaUser : fatura.kesenFirma.earsivUser,
                efaturaPwd: fatura.firma.eFaturaMi ? fatura.kesenFirma.efaturaPwd : fatura.kesenFirma.earsivPwd,
            },
            faturaKodu: faturaKodu,
            sonKesilenFaturaNo: sonFaturaNo
        }

        return faturaKesim
    }
    catch (e) {
        logger.log('crit', e)
    }
}
*/

//-----------------------------------------------------------------------------------------------------------------------------------------

const updateFatura = async (key, data) => {
    logger.info(`Fatura guncelleniyor.`)
    await FaturaModel.findOneAndUpdate({ key: key }, data).exec()
}

//-----------------------------------------------------------------------------------------------------------------------------------------

const sendToBank = async (kesim) => {
    try {
        logger.info(`QNB baglantisi kuruluyor: ${kesim.baglanti.url} ${kesim.baglanti.user} ${kesim.baglanti.pwd}`)

        let client = await QnbService.createClient(kesim.baglanti.url, kesim.baglanti.user, kesim.baglanti.pwd)
        if (!client) {
            logger.log('crit', `QNB baglantisi kurulamadi. ${kesim.baglanti.url} ${kesim.baglanti.user} ${kesim.baglanti.pwd}`)
            // TODO
            return {
                error: {
                    code: -1,
                    message: `Baglanti kurulamadi ${kesim.baglanti.url} ${kesim.baglanti.user} ${kesim.baglanti.pwd}`
                }
            }
        }
        logger.info(`Qnb baglantisi kuruldu`)

        // Karşı taraf eFatura mı, değilse eArşiv kesilir.
        logger.info(`EFatura mi sorgulanacak: ${kesim.fatura.firma.vergiNo}`)
        let eFaturaMi = await client.efaturaKullanicisiMi(kesim.fatura.firma.vergiNo)
        logger.info(`EFatura mi: ${JSON.stringify(eFaturaMi, null, 2)}`)

        // Yeni Fatura no al
        ///let faturaNo = ''
        ///let tryLimit = 3
        ///do {
        ///    logger.info(`FaturaNoUret(): Kesen Firma VKN:${kesim.fatura.kesenFirma.vkn}, Fatura Kodu: ${kesim.faturaKodu}`)
        ///    faturaNo = await client.faturaNoUret(kesim.fatura.kesenFirma.vkn, 'CCC')//kesim.faturaKodu)
        ///    if (--tryLimit <= 0) {
        ///        // 
        ///        logger.log('crit', `Yeni Fatura numarasi alinamiyor. Alinan yeni fatura numarasi: ${faturaNo}, son fatura no: ${kesim.sonKesilenFaturaNo}`)
        ///        return {
        ///            error: {
        ///                code: -2,
        ///                message: `Gelen Fatura kullanilmis ${faturaNo}`
        ///            }
        ///        }
        ///    }
        ///} while (faturaNo == kesim.sonKesilenFaturaNo)
        ///logger.info(`Fatura duzenleme tarihi: ${moment(kesim.fatura.duzenlemeTarihi).format('DD.MM.YYYY')}, Fatura Seri: ${kesim.faturaKodu}, Son Fatura No: ${kesim.sonKesilanFaturaNo}, Alinan Fatura No: ${faturaNo}`)
        ///
        ///// Yeni alinan Fatura no atanir
        ///kesim.fatura.faturaNo = faturaNo




        let ubl = makeUbl(kesim.fatura)
        logger.info(`UBL: ${ubl}`)
        let belgeOid = await client.belgeGonder({ ubl: ubl, vkn: kesim.fatura.kesenFirma.vkn, key: kesim.fatura.key })
        //let gonderimDurumu
        logger.info(`EFatura gonderildi FaturaID: ${kesim.fatura._id}  BelgeOID: ${belgeOid}.`);
        if (belgeOid) {
            return {
                belge: {
                    belgeOid: belgeOid
                }
            }
        }

        //do {
        //    await new Promise(resolve => setTimeout(resolve, 5000))
        //
        //    gonderimDurumu = await client.gidenBelgeSorgula(kesim.fatura.kesenFirma.vkn, belgeOid)
        //    console.log(gonderimDurumu)
        //    if (gonderimDurumu.durum == 1) {
        //        logger.info(`Belge henuz islenmedi. Bekle ...`)
        //    }
        //    else if (gonderimDurumu.durum == 2) {
        //        // TODO
        //        logger.log(`crit`, `Belge Gonderim Durumu ${gonderimDurumu.durum} : ${gonderimDurumu.aciklama}, Alim Tarihi: ${moment(gonderimDurumu.alimTarihi, 'YYYYMMDDHHmmSS').format('HH:mm DD.MM.YYYY')}`)
        //        return {
        //            error: {
        //                code: -31,
        //                message: gonderimDurumu.aciklama
        //            }
        //        }
        //    }
        //    else if (gonderimDurumu.durum == 3) {
        //        logger.info(`Belge basarili islendi.`)
        //        break
        //    }
        //} while (gonderimDurumu.durum == 1)

        // Fatura Gonderildi
        //logger.info(`EFatura gonderildi FaturaID: ${fatura._id}  BelgeOID: ${belgeOid}.`);
        //logger.info(`EFatura gonderim cevabi: ${JSON.stringify(gonderimDurumu, null, 2)}`)
        //return {
        //    error: null,
        //    belge: {
        //        belgeOid: belgeOid,
        //        alimTarihi: gonderimDurumu.alimTarihi,
        //        belgeNo: gonderimDurumu.belgeNo,
        //        ettn: gonderimDurumu.ettn,
        //        olusturulmaTarihi: gonderimDurumu.olusturulmaTarihi,
        //        faturaNo: gonderimDurumu.belgeNo,
        //        ubl: ubl
        //    }
        //}
    }
    catch (e) {
        logger.log(`crit`, e)
        return {
            error: {
                code: -30,
                message: e
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------

const _sendEFatura = async (efatura) => {
    try {
        logger.info(`EFatura gonderim talebi geldi. ${JSON.stringify(efatura)}`);

        let fatura = await prepareFatura(efatura)
        logger.info(`Fatura okundu. Gonderilecek ${fatura}`)
        if (!fatura) {
            return {
                error: {
                    code: -10,
                    message: 'Fatura hazirlanamadi'
                }
            }
        }

        let resp
        if (fatura.fatura.firma.eFaturaMi) {
            logger.info(`EFatura gonderilecek`)
            resp = await sendToBank(fatura)
        }
        else {
            logger.info(`EArsiv gonderilecek`)
            resp = await SendEArsivToBank(fatura)
        }

        if (resp.error) {
            logger.log('crit', `Hata: ${resp.error.code}, ${resp.error.message}`)
            return {
                error: {
                    code: resp.error.code,
                    message: resp.error.message
                }
            }
        }
        else {
            let updateData = {
                belgeOid: resp.belge.belgeOid,
                alimTarihi: resp.belge.alimTarihi,
                belgeNo: resp.belge.belgeNo,
                ettn: resp.belge.ettn,
                olusturulmaTarihi: resp.belge.olusturulmaTarihi,
                faturaNo: resp.belge.faturaNo,
                ubl: resp.belge.ubl,
                status: 'Gonderildi'
            }
            await updateFatura(efatura.key, updateData)
        }
        return {
            error: null,
            result: {
                key: efatura.key
            }
        }
        //efaturaChannel.ack(efaturaData)
    } catch (e) {
        logger.log(`crit`, e)
        return {
            error: {
                code: -11,
                message: e
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------

const gidenBelgeleriSorgula = async (data) => {
    try {
        let { faturalar, vkn, url, user, pwd } = data
        logger.info(`QNB baglantisi kuruluyor: ${url} ${user} ${pwd}`)

        let client = await QnbService.createClient(url, user, pwd)
        if (!client) {
            logger.log('crit', `QNB baglantisi kurulamadi. ${url} ${user} ${pwd}`)
            return {
                error: {
                    code: -1,
                    message: `Baglanti kurulamadi ${url} ${user} ${pwd}`
                }
            }
        }
        logger.info(`Qnb baglantisi kuruldu`)

        for (let fatura of faturalar) {
            let result = await client.gidenBelgeSorgula(vkn, fatura.belgeOid)
            if (result.durum == 1) {
                logger.info(`Fatura: ${fatura.key} : ${fatura.belgeOid}, Durum ${result.durum}, Bekle`)
            }
            else if (result.durum == 2) {
                logger.info(`Fatura: ${fatura.key} : ${fatura.belgeOid}, Durum ${result.durum}, Hata, ${result.aciklama}`)
            }
            else if (result.durum == 3) {
                logger.info(`Fatura: ${fatura.key} : ${fatura.belgeOid}, Durum ${result.durum}, Banka tarafindan kabul edilmis`)
                if (result.gonderimDurumu == 2) {
                    logger.info(`Fatura: ${fatura.key} : ${fatura.belgeOid}, Durum ${result.gonderimDurumu}, GIB'e gonderildi`)
                }
                else if (result.gonderimDurumu == 3) {
                    logger.info(`Fatura: ${fatura.key} : ${fatura.belgeOid}, Durum ${result.gonderimDurumu}, GIB Merkez yaniti geldi`)
                }
                else if (result.gonderimDurumu == 4) {
                    if (result.yanitDurumu == -1) {
                        logger.info(`Fatura: ${fatura.key} : ${fatura.belgeOid}, Durum ${result.yanitDurumu}, Alicidan yanit beklenmiyor`)
                        logger.info(`Fatura belgesi aliniyor. ${fatura.key} : ${fatura.belgeOid}`)
                        let gidenBelgePdf = await client.gidenBelgeleriIndir({
                            vergiTcKimlikNo: vkn,
                            belgeOidListesi: [fatura.belgeOid],
                            belgeTuru: 'FATURA',
                            belgeFormati: 'PDF'
                        })
                        logger.info(`Fatura belgesi alindi. ${fatura.key} : ${fatura.belgeOid}`)
                        await updateFatura(fatura.key, { gidenBelgePdf: gidenBelgePdf, status: 'BasariliKesildi' })
                    }
                    else if (result.yanitDurumu == 0) {
                        logger.info(`Belge: ${belge}, Durum ${result.yanitDurumu}, Alicidan yanit bekleniyor`)
                    }
                    else if (result.yanitDurumu == 1) {
                        logger.info(`Belge: ${belge}, Durum ${result.yanitDurumu}, RET`)
                    }
                    else if (result.yanitDurumu == 2) {
                        logger.info(`Belge: ${belge}, Durum ${result.yanitDurumu}, Kabul`)
                    }
                }
            }
        }

        return {
            error: null,
            result: {}
        }
    }
    catch (e) {
        return {
            error: {
                code: -12,
                message: e
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------

const gelenFaturalariAl = async ({ vkn, sonAlinanBelgeSiraNo, belgeTuru, url, user, pwd }) => {
    try {
        logger.info(`QNB baglantisi kuruluyor: ${url} ${user} ${pwd}`)

        let client = await QnbService.createClient(url, user, pwd)
        if (!client) {
            logger.log('crit', `QNB baglantisi kurulamadi. ${url} ${user} ${pwd}`)
            return {
                error: {
                    code: -1,
                    message: `Baglanti kurulamadi ${url} ${user} ${pwd}`
                }
            }
        }
        logger.info(`Qnb baglantisi kuruldu`)
        logger.info(`Gelen Faturlar alinacak. VKN: ${vkn}`)
        let result = await client.gelenBelgeleriAl({ vkn, sonAlinanBelgeSiraNo, belgeTuru })
        if (result.error) {
            return {
                error: {
                    code: -14,
                    message: result.error.message
                }
            }
        }
        else {
            for (let belge of result) {
                let xmlObj = await Xml.parseStringPromise(gelenBelge.belgeVerisi)

            }
        }
    }
    catch (e) {
        logger.log(`crit`, e)
        return {
            error: {
                code: -13,
                message: e
            }
        }
    }
}

module.exports = {
    sendEFatura: sendToBank,
    //sendEFatura: sendEFatura,
    //sendToBank,
    gidenBelgeleriSorgula,
    gelenFaturalariAl
}
