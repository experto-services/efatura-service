EArsiv:
    Portal: https://portaltest.efinans.com.tr/yonetim-side/dashboard.jsp
        VKN: 3810685526
        Kullanıcı Kodu: experto.eardef
        Şifre: 12345678aB

    https://earsivtest.efinans.com.tr/earsiv/ws/EarsivWebService?wsdl


EArsiv alici: 
    vkn: 9205121120
    adres: Papatya Caddesi Yasemin Sokak

Test Mesajlari:
Fatura Kesim:
{
    "command":"FATURA_KES",
    "payload": {
        "key":"706a36f8-8466-461c-9958-57c93d69fd49"
    }
}





EArsiv Kesim
{
    "command": "FATURA_KES",
    "payload": {
        "key": "f62fa6ec-bfed-4ec0-b2d8-e50a2d4c9600"
    }
}

Fatura Sorgulama: 
{
    "command" : "FATURA_SORGULA",
    "payload": {
        "vkn"  : "3810685526",
        "url"  : "https://erpefaturatest.cs.com.tr:8043/efatura/ws/connectorService?wsdl",
        "user" : "3810685526",
        "pwd"  : "Experto2021",
        "faturalar": [
            {
                "key"      : "706a36f8-8466-461c-9958-57c93d69fd49",
                "belgeOid" : "0nkocurylt14a1"
            },
            {
                "key"      : "61977ecf-9692-41d7-a7d8-f413c1ec1699",
                "belgeOid" : "0nkocurylt146y"
            }
        ]
    }
}

Gelen Faturalari Al: 
{
    "command" : "GELEN_FATURALARI_AL",
    "payload": {
        "vkn"                   : "3810685526",
        "sonAlinanBelgeSiraNo"  : "",
        "belgeTuru"             : "FATURA",
        "url"                   : "https://erpefaturatest.cs.com.tr:8043/efatura/ws/connectorService?wsdl",
        "user"                  : "3810685526",
        "pwd"                   : "Experto2021"
    }
}